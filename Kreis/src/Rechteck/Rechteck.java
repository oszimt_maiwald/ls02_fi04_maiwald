package Rechteck;

import java.awt.Point;

public class Rechteck {

	private double hoehe;
	private double breite;
	private Point mittelpunkt;
    
	public Rechteck(double hoehe, double breite, Point mittelpunkt)
	{
		setHoehe(hoehe);
		setBreite(breite);
		setMittelpunkt(mittelpunkt);
	}
	
	public void setHoehe(double hoehe) {
        if(hoehe < 0) {
            this.hoehe = 0;
        }else {
            this.hoehe = hoehe;
        }
    }
    
    public void setBreite(double breite)  {
        if(breite < 0) {
            this.breite = 0;
        }else {
            this.breite = breite;
        }
    }
	
	public double getHoehe() {
		return this.hoehe;
	}
	
	public double getBreite() {
		return this.breite;
	}
	
	public double getDurchmesser() {
		return Math.sqrt(Math.pow(this.hoehe, 2) + Math.pow(this.breite, 2));
	}
	
	public double getFlaeche() {
		return this.hoehe * this.breite;
	}
	
	public double getUmfang() {
		return 2.0*(this.hoehe+this.breite);
	}
	
	public void setMittelpunkt(Point mittelpunkt) {
		this.mittelpunkt = mittelpunkt; 
	}
	
	public Point getMittelpunkt() {
		return this.mittelpunkt;
	}
}
