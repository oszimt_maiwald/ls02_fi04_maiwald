package angestelltenverwaltung;

public class Angestellter {
	//Attribute
	private String name;
	private double gehalt;
	
	//Methoden
	public void setName(String name) {
		this.name = name;		
	}
	
	public void getName() {
		return this.name;		
	}
	
	public void setGehalt(double gehalt) {
		this.gehalt = gehalt;
	}
	
	public void getGehalt() {
		return this.gehalt;
	}
}
