package KriegDerRaumschiffe;

public class Ladung {
	//Attribute
	private String bezeichnung; 
	private double menge;
	
	//Konstruktoren
	public Ladung() {
		this.bezeichnung = "Keine Ladung vorhanden!";
		this.menge = 0;
		
	}
	public Ladung(String bezeichnung, double menge) {
		setBezeichnung(bezeichnung);
		setMenge(menge);
	}
	
	//Getter und Setter
	public String getBezeichnung() {
		return this.bezeichnung;
	}
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}
	public double getMenge() {
		return this.menge;
	}
	public void setMenge(double menge) {
		this.menge = menge;
	}
	
}
