package KriegDerRaumschiffe;

import java.util.ArrayList;
import java.util.Scanner;

public class Raumschiff {
	//Attribute
	private double photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private double androidenAnzahl;
	private String schiffsname;
	private ArrayList<String> broadcastKommunikator;
	private ArrayList<Ladung> ladungsverzeichnis;
	
	//Konstruktoren
	public Raumschiff() {
		this.schiffsname = "Es ist kein Schiff definiert!";
		this.photonentorpedoAnzahl = 0;
		this.energieversorgungInProzent = 0;
		this.schildeInProzent = 0;
		this.huelleInProzent = 0;
		this.lebenserhaltungssystemeInProzent = 0;
		this.androidenAnzahl = 0;
		setBroadcastKommunikator(new ArrayList<String>());
		setLadungsverzeichnis(new ArrayList<Ladung>());
		
	}
	public Raumschiff(double photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent, int huelleInProzent, int lebenserhaltungssystemeInProzent, double androidenAnzahl, String schiffsname) {
		setPhotonentorpedoAnzahl(photonentorpedoAnzahl);
		setEnergieversorgungInProzent(energieversorgungInProzent);
		setSchildeInProzent(schildeInProzent);
		setHuelleInProzent(huelleInProzent);
		setLebenserhaltungssystemeInProzent(lebenserhaltungssystemeInProzent);
		setAndroidenAnzahl(androidenAnzahl);
		setSchiffsname(schiffsname);
		setBroadcastKommunikator(new ArrayList<String>());
		setLadungsverzeichnis(new ArrayList<Ladung>());
	}
	
	
	//Getter und Setter
	public double getPhotonentorpedoAnzahl() {
		return this.photonentorpedoAnzahl;
	}
	public void setPhotonentorpedoAnzahl(double photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}
	public int getEnergieversorgungInProzent() {
		return this.energieversorgungInProzent;
	}
	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}
	public int getSchildeInProzent() {
		return this.schildeInProzent;
	}
	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}
	public int getHuelleInProzent() {
		return this.huelleInProzent;
	}
	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}
	public int getLebenserhalungssystemeInProzent() {
		return this.lebenserhaltungssystemeInProzent;
	}
	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}
	public double getAndroidenAnzahl() {
		return this.androidenAnzahl;
	}
	public void setAndroidenAnzahl(double androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}
	public String getSchiffsname() {
		return this.schiffsname;
	}
	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}
	public ArrayList<String> getBroadcastKommunikator(){
		return this.broadcastKommunikator;
	}
	public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		this.broadcastKommunikator = broadcastKommunikator;
	}
	public ArrayList<Ladung> getLadungsverzeichnis(){
		return this.ladungsverzeichnis;
	}
	public void setLadungsverzeichnis (ArrayList<Ladung> ladungsverzeichnis) {
		this.ladungsverzeichnis = ladungsverzeichnis;
	}
	
	//Methoden
	/**Diese Methode fügt mithilfe des Übergabeparameters neueLadung eine neue Ladung der ArrayList "ladungsverzeichnis" hinzu.
	 * @param neueLadung Übergabeparameter für die Ladung, um Ladungen Raumschiffen zuweisen zu können. */
	public void addLadung(Ladung neueLadung) {
		this.ladungsverzeichnis.add(neueLadung);
	}
	/**Diese Methode gibt einen gesamten Statusbericht in der Konsole aus. */
	public void ausgabeZustandInKonsole() {
		System.out.println(">>>>Statusbericht Anfang<<<<");
		System.out.println("Schiffsname: " + this.getSchiffsname());
		System.out.println("Energieversorgung: " + this.getEnergieversorgungInProzent() + "%");
		System.out.println("Schilde: " + this.getSchildeInProzent() + "%");
		System.out.println("Hülle: " + this.getHuelleInProzent() + "%");
		System.out.println("Lebenserhaltungssysteme: " + this.getLebenserhalungssystemeInProzent() + "%");
		System.out.println("Anzahl Androiden: " + this.getAndroidenAnzahl());
		System.out.println("Anzahl Photonentorpedos: " + this.getPhotonentorpedoAnzahl());
		System.out.println(">>>>Statusbericht Ende<<<<");
	}
	/**Diese Methode gibt für jedes Objekt "Ladung" in der ArrayList "ladungsverzeichnis" die Bezeichnung und die Menge aus. */
	public void ausgabeLadungsverzeichnis() {
		for(Ladung ladung : this.ladungsverzeichnis) {
			System.out.println("Bezeichnung der Ladung: " + ladung.getBezeichnung());
			System.out.println("Anzahl der Ladung: " + ladung.getMenge());
		}				
	}
	

	/**Diese Methode schießt Photonentorpedos auf das Ziel, welches als Übergabeparameter übergeben wird.
	   Zuerst wird überprüft, ob Photonentorpedos vorhanden sind. Sind keine vorhanden, dann wird eine Nachricht der ArrayList "broadcastKommunikator" hinzugefügt und in der Konsole ausgegeben.
	   Sind Torpedos vorhanden, dann wird die Anzahl der Torpedos um eins reduziert, eine Nachricht wird der ArrayList "broadcastKommunkator" hinzugefügt und es wird die Methode trefferVermerken() aufgerufen. 
	   @param ziel Übergabeparameter fpr das Zielraumschiff.*/
	public void photonentorpedosAbschießen(Raumschiff ziel) {
		if(this.getPhotonentorpedoAnzahl() == 0) {
			this.addBroadcast("-=*Click*=-");
			ausgabeBroadcast();
		}
		else {
			this.setPhotonentorpedoAnzahl(this.getPhotonentorpedoAnzahl()-1);
			this.addBroadcast("Photonentorpedo abgeschossen!");
			ausgabeBroadcast();
			trefferVermerken(ziel);
		}
	}
	/**Diese Methode schießt Phaserkanonen auf das Ziel, welches als Übergabeparameter übergeben wird.
	   Zuerst wird überprüft, ob genug Energie vorhanden ist. Ist nicht genug vorhanden, dann wird eine Nachricht der ArrayList "broadcastKommunikator" hinzugefügt und in der Konsole ausgegeben.
	   Ist genug vorhanden, dann wird die Energieversorgung um 50% reduziert, eine Nachricht wird der ArrayList "broadcastKommunkator" hinzugefügt und es wird die Methode trefferVermerken() aufgerufen. 
	   @param ziel Übergabeparameter für das Zielraumschiff.*/
	public void phaserkanonenAbschießen(Raumschiff ziel) {
		if(this.getEnergieversorgungInProzent() < 50) {
			addBroadcast("-=*Click*=-");
			this.ausgabeBroadcast();
		}
		else {
			this.setEnergieversorgungInProzent(this.getEnergieversorgungInProzent() - 50);
			addBroadcast("Phaserkanone abgeschossen!");
			this.ausgabeBroadcast();
			trefferVermerken(ziel);
		}
	}
	/**Diese Methode vermerkt Treffer in der ArrayList "broadcastKommunikator" und gibt diese in der Konsole aus. Ebenfalls wird der Schaden am Zielraumschiff festgelegt.
	 * @param ziel Übergabeparameter für das Zielraumschiff */
	public void trefferVermerken(Raumschiff ziel) {
		addBroadcast(ziel.getSchiffsname() + " wurde getroffen!");
		this.ausgabeBroadcast();
		ziel.setSchildeInProzent(ziel.getSchildeInProzent() - 50);
		if (ziel.getSchildeInProzent() <= 0) {
			ziel.setHuelleInProzent(ziel.getHuelleInProzent() - 50);
			ziel.setEnergieversorgungInProzent(ziel.getEnergieversorgungInProzent() -50);
		}
		if (ziel.getHuelleInProzent() <= 0) {
			ziel.setLebenserhaltungssystemeInProzent(0);
			this.addBroadcast("Die Lebenserhaltungssysteme wurden vernichtet!");
			this.ausgabeBroadcast();
		}
	}
	/**Diese Methode speichert einen String unter einem neuen index in der ArrayList "broadcastKommunikator".
	 * @param neuerBroadcast Übergabeparameter für den einzuspeichernden Text.   */
	public void addBroadcast(String neuerBroadcast) {
		this.broadcastKommunikator.add(neuerBroadcast);
	}
	/**Diese Methode gibt den letzten Eintrag in der ArrayList "broadcastKommunikator" aus. */
	public void ausgabeBroadcast() {		
		System.out.println(this.broadcastKommunikator.get(broadcastKommunikator.size() -1));
	}
	/**Diese Methode gibt alle Einträge der ArrayList "broadcastKommunikator" in der Konsole aus. */
	public void ausgabeLog() {
		System.out.println(this.broadcastKommunikator);
	}
	/**Diese Methode gibt den Status des erstellten Raumschiffes in der Konsole wieder. Diese Methode wird verwendet, nachdem ein Objekt der Klasse "Raumschiff" mithilfe des Konstruktors erstellt wurde. */
    public void erstelleRaumschiff() {		
        System.out.println("Schiffname: " + this.getSchiffsname());
        System.out.println("Photonentorpedos: " + this.getPhotonentorpedoAnzahl());
        System.out.println("Anzahl Androiden " + this.getAndroidenAnzahl());
        System.out.println("Energie: " + this.getEnergieversorgungInProzent()+"%");
        System.out.println("Schild: " + this.getSchildeInProzent()+"%");
        System.out.println("Hülle: " + this.getHuelleInProzent()+"%");
        System.out.println("Lebenserhaltungssysteme: " + this.getLebenserhalungssystemeInProzent() );
        System.out.println("=======================");
	}
	
}
