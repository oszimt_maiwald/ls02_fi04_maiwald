package KriegDerRaumschiffe;

public class TestRaumschiff{
	public static void main(String[] args) {
		//erstelle Raumschiff der Klingonen
        Raumschiff r = new Raumschiff(1.0, 100, 100, 100, 100, 2.0, "IKS Hegh'ta");
        r.erstelleRaumschiff();
		//erstelle Raumschiff der Romulaner
        Raumschiff p = new Raumschiff(2.0, 100, 100, 100, 100, 2.0, "IRW Khazara");
        p.erstelleRaumschiff();
        //erstelle Raumschiff der Vulkanier
        Raumschiff s = new Raumschiff(0.0, 100, 100, 100, 100, 5.0, "Ni'Var");
        s.erstelleRaumschiff();
        
        //erstelle Ladungen
        Ladung q = new Ladung("Ferengi Schneckensaft", 200);
        Ladung w = new Ladung("Bat'leth Klingonen Schwert", 200);
        Ladung e = new Ladung("Borg-Schrott", 5);
        Ladung t = new Ladung("Rote Materie", 2);
        Ladung z = new Ladung("Plasma-Waffe", 50);
        Ladung a = new Ladung("Forschungssonde", 35);
        Ladung l = new Ladung("Photonentorpedo", 3);
        
        //Ladungen werden den Raumschiffen zugeordnet
        r.addLadung(q);
        r.addLadung(w);
        p.addLadung(e);
        p.addLadung(t);
        p.addLadung(z);
        s.addLadung(a);
        s.addLadung(l);
        
        
        //Auszuführende Methoden laut Aufgabe
        r.photonentorpedosAbschießen(p);
        p.phaserkanonenAbschießen(r);
        s.addBroadcast(s.getSchiffsname() + ": Gewalt ist nicht logisch");
        s.ausgabeBroadcast();
        r.ausgabeZustandInKonsole();
        r.ausgabeLadungsverzeichnis();
        r.photonentorpedosAbschießen(p);
        r.photonentorpedosAbschießen(p);
        r.ausgabeZustandInKonsole();
        r.ausgabeLadungsverzeichnis();
        p.ausgabeZustandInKonsole();
        p.ausgabeLadungsverzeichnis();
        s.ausgabeZustandInKonsole();
        s.ausgabeLadungsverzeichnis();
        
        
        
        
//        r.photonentorpedosAbschießen(p);
//        r.ausgabeZustandInKonsole();
//        p.ausgabeZustandInKonsole();
//        r.photonentorpedosAbschießen(p);
//        r.ausgabeZustandInKonsole();
//        p.ausgabeZustandInKonsole();
//        r.photonentorpedosAbschießen(p);
//        r.ausgabeZustandInKonsole();
//        p.ausgabeZustandInKonsole();
        
	}
        // r.ausgabeLog();
}

